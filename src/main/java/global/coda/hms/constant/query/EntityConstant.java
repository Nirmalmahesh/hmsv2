package global.coda.hms.constant.query;

public final class EntityConstant {
  public static final String USERNAME = "username";
  public static final String PASSWORD = "password";
  public static final String FIRSTNAME = "firstname";
  public static final String LASTNAME = "lastname";
  public static final String USER_ID = "user_id";
  public static final String PATIENT_ID = "patient_id";
  public static final String WEIGHT = "weight";
  public static final String BLOOD_GROUP = "blood_group";
  public static final String CITY = "city";
  public static final String STATE = "state";
  public static final String PHONE_NUMBER = "phone_number";
  public static final String PK_DOCTOR_ID = "pk_doctor_id";
  public static final String FK_ROLE_ID = "fk_role_id";
  public static final String CREATED_TIME = "created_time";
  public static final String UPDATED_TIME = "updated_time";
  public static final String SPECIALIZATION = "doctor_specialization";
}
