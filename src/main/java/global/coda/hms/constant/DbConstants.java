package global.coda.hms.constant;

/**
 * Maintain the db Constants .
 * @author Nirmlamahesh S
 */
public final class DbConstants {
  public static final String URL = "jdbc:mysql://localhost:3306/hms";
  public static final String USERNAME = "sa";
  public static final String PASSWORD = "joker";
  public static final String DRIVER_CLASS = "com.mysql.cj.jdbc.Driver";


  /**
   * constructor .
   */
  private DbConstants() {
  }
}
