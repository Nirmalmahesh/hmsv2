package global.coda.hms.dao;


import global.coda.hms.config.DbConfig;
import global.coda.hms.constant.ApplicationConstant;
import global.coda.hms.constant.NumericConstants;
import global.coda.hms.constant.query.DoctorQuries;
import global.coda.hms.constant.query.EntityConstant;
import global.coda.hms.constant.query.UserDetailsQueries;
import global.coda.hms.exception.doctor.DoctorNotFoundException;
import global.coda.hms.model.Doctor;
import global.coda.hms.model.Patient;
import global.coda.hms.pojo.DoctorPatientMapper;
import global.coda.hms.util.DaoUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Doctor.
 *
 * @author Nirmalmahesh Subramani
 */
public class DoctorDao {


  private static final Logger LOGGER = LogManager.getLogger(DoctorDao.class);

  /**
   * to create the doctor.
   *
   * @param doctor to create
   * @return the doctor
   * @throws SQLException           the sql exception
   * @throws InstantiationException the instantiation exception
   * @throws IllegalAccessException the illegal access exception
   * @throws ClassNotFoundException the class not found exception
   */
  public Doctor createDoctor(Doctor doctor) throws SQLException, InstantiationException,
          IllegalAccessException, ClassNotFoundException {


    LOGGER.traceEntry(doctor.toString());
    Connection dbConnection = DbConfig.getHmsDaoConnection();
    PreparedStatement userDetailCreatePreparedStatment = null;
    PreparedStatement doctorCreatePreparedStatement = null;
    try {
      dbConnection.setAutoCommit(false);
      userDetailCreatePreparedStatment =
              dbConnection.prepareStatement(UserDetailsQueries.USER_DETAIL_INSERT,
                      Statement.RETURN_GENERATED_KEYS);
      DaoUtil.preparedStatment(userDetailCreatePreparedStatment, doctor);
      int numRowsAffectedUser = userDetailCreatePreparedStatment.executeUpdate();
      if (numRowsAffectedUser == 1) {
        int generatedUserId = DaoUtil.getGeneratedPrimaryKey(userDetailCreatePreparedStatment);
        doctor.setUserId(generatedUserId);
        doctorCreatePreparedStatement = dbConnection.prepareStatement(DoctorQuries.DOCTOR_INSERT,
                Statement.RETURN_GENERATED_KEYS);
        setDoctorParameters(doctorCreatePreparedStatement, doctor);
        doctorCreatePreparedStatement.executeUpdate();
        int generatedKeyDoctor = DaoUtil.getGeneratedPrimaryKey(doctorCreatePreparedStatement);
        doctor.setDoctorId(generatedKeyDoctor);
        dbConnection.commit();
      } else {
        dbConnection.rollback();
      }

    } catch (SQLException exception) {
      throw new SQLException(exception);
    } finally {
      if (userDetailCreatePreparedStatment != null) {
        userDetailCreatePreparedStatment.close();
      }
      if (doctorCreatePreparedStatement != null) {
        doctorCreatePreparedStatement.close();
      }
    }
    LOGGER.traceExit();
    return doctor;
  }

  /**
   * used for assign the doctor parameters.
   *
   * @param preparedStatementCreateDoctor for set doctor parameters
   * @param doctor                        to be created
   * @throws SQLException when statment closed
   */
  private void setDoctorParameters(PreparedStatement preparedStatementCreateDoctor,
                                   Doctor doctor) throws SQLException {
    preparedStatementCreateDoctor.setString(NumericConstants.ONE,
            doctor.getDoctor_specialization());
    preparedStatementCreateDoctor.setInt(NumericConstants.TWO, doctor.getUserId());
  }

  /**
   * To get all doctors .
   *
   * @return the list of doctors
   * @throws SQLException            while executing the query
   * @throws DoctorNotFoundException while no doctors found
   * @throws InstantiationException  the instantiation exception
   * @throws IllegalAccessException  the illegal access exception
   * @throws ClassNotFoundException  the class not found exception
   */
  public List<Doctor> getAllDoctors() throws SQLException, DoctorNotFoundException,
          InstantiationException, IllegalAccessException, ClassNotFoundException {
    LOGGER.traceEntry();
    Connection hospitalDbConnection = DbConfig.getHmsDaoConnection();
    PreparedStatement preparedStatement;
    preparedStatement = hospitalDbConnection.prepareStatement(DoctorQuries.DOCTOR_SELECT_ALL);
    ResultSet doctorResultSet = preparedStatement.executeQuery();

    List<Doctor> allDoctors = readDoctorFromResultSet(doctorResultSet);
    LOGGER.traceExit(allDoctors);
    return allDoctors;


  }

  /**
   * read doctor from the resultset .
   *
   * @param resultSet resultset of an doctor
   * @return list of doctos
   * @throws SQLException            when query fails
   * @throws DoctorNotFoundException when no doctors found
   */

  private List<Doctor> readDoctorFromResultSet(ResultSet resultSet) throws SQLException,
          DoctorNotFoundException {
    LOGGER.traceEntry(resultSet.toString());
    List<Doctor> doctors = new ArrayList<>();
    while (resultSet.next()) {
      Doctor tempDoctor = new Doctor();
      tempDoctor.setUserId(resultSet.getInt(EntityConstant.USER_ID));
      tempDoctor.setUsername(resultSet.getString(EntityConstant.USERNAME));
      tempDoctor.setPassword(resultSet.getString(EntityConstant.PASSWORD));
      tempDoctor.setUserFirstName(resultSet.getString(EntityConstant.FIRSTNAME));
      tempDoctor.setUserLastName(resultSet.getString(EntityConstant.LASTNAME));
      tempDoctor.setDoctorId(resultSet.getInt(EntityConstant.PK_DOCTOR_ID));
      tempDoctor.setDoctor_specialization(resultSet.getString(EntityConstant.SPECIALIZATION));
      tempDoctor.setRoleId(resultSet.getInt(EntityConstant.FK_ROLE_ID));
      tempDoctor.setUserCity(resultSet.getString(EntityConstant.CITY));
      tempDoctor.setCreatedTime(resultSet.getTimestamp(EntityConstant.CREATED_TIME));
      tempDoctor.setUpdatedTime(resultSet.getTimestamp(EntityConstant.UPDATED_TIME));
      tempDoctor.setUserState(resultSet.getString(EntityConstant.STATE));
      doctors.add(tempDoctor);
    }
    LOGGER.traceExit(doctors);
    return doctors;
  }

  /**
   * get doctor from the docter id.
   *
   * @param doctorId the doctor id
   * @return doctor object
   * @throws SQLException            the sql exception
   * @throws DoctorNotFoundException the doctor not found exception
   * @throws InstantiationException  the instantiation exception
   * @throws IllegalAccessException  the illegal access exception
   * @throws ClassNotFoundException  the class not found exception
   */
  public Doctor getDoctor(int doctorId) throws SQLException, DoctorNotFoundException,
          InstantiationException, IllegalAccessException, ClassNotFoundException {
    LOGGER.traceEntry(Integer.toString(doctorId));
    Connection hospitalDbConnection = DbConfig.getHmsDaoConnection();
    hospitalDbConnection.setAutoCommit(false);
    PreparedStatement readPatientPreparedStatement = null;
    ResultSet resultSetPatient = null;
    List<Doctor> doctors;
    try {
      readPatientPreparedStatement =
              hospitalDbConnection.prepareStatement(DoctorQuries.DOCTOR_SELECT_BY_ID);
      readPatientPreparedStatement.setInt(1, doctorId);
      resultSetPatient = readPatientPreparedStatement.executeQuery();
      doctors = readDoctorFromResultSet(resultSetPatient);
      hospitalDbConnection.commit();
    } catch (SQLException exception) {
      throw new SQLException(exception);
    } finally {
      if (readPatientPreparedStatement != null) {
        readPatientPreparedStatement.close();
      }
      if (resultSetPatient != null) {
        resultSetPatient.close();
      }
    }
    LOGGER.traceExit();
    if (doctors.size() > 0) {
      LOGGER.traceExit(doctors.get(NumericConstants.ZERO));
      return doctors.get(0);
    } else {
      return null;
    }

  }

  /**
   * Delete the doctor using the doctor id .
   *
   * @param doctorId the doctor id
   * @return the boolean value
   * @throws SQLException            when query execution fails
   * @throws DoctorNotFoundException doctor not found
   * @throws InstantiationException  the instantiation exception
   * @throws IllegalAccessException  the illegal access exception
   * @throws ClassNotFoundException  the class not found exception
   */
  public boolean deleteDoctor(int doctorId) throws SQLException, DoctorNotFoundException,
          InstantiationException, IllegalAccessException, ClassNotFoundException {
    LOGGER.traceEntry();
    Connection dbConnection = DbConfig.getHmsDaoConnection();
    dbConnection.setAutoCommit(false);

    PreparedStatement deletePatientStatement =
            dbConnection.prepareStatement(DoctorQuries.DOCTOR_DELETE_BY_ID);
    deletePatientStatement.setInt(NumericConstants.ONE, doctorId);
    int affectedRows = deletePatientStatement.executeUpdate();
    PreparedStatement deleteUserStatement =
            dbConnection.prepareStatement(UserDetailsQueries.USER_DELETE_BY_ID);

    if (affectedRows == 1) {
      dbConnection.commit();
      LOGGER.traceExit();
      return true;
    } else {
      dbConnection.rollback();
      LOGGER.traceExit();
      return false;
    }


  }

  /**
   * used fpor update the doctor.
   *
   * @param newDoctor newdoc
   * @return boolean boolean
   * @throws SQLException            while query fails
   * @throws DoctorNotFoundException when no doctor found
   * @throws InstantiationException  the instantiation exception
   * @throws IllegalAccessException  the illegal access exception
   * @throws ClassNotFoundException  the class not found exception
   */
  public boolean updateDoctor(Doctor newDoctor) throws SQLException, DoctorNotFoundException,
          InstantiationException, IllegalAccessException, ClassNotFoundException {
    Doctor oldDoctor = getDoctor(newDoctor.getDoctorId());
    Connection hospitalDbConnection = DbConfig.getHmsDaoConnection();
    if (oldDoctor != null) {
      PreparedStatement userDetailUpdatePreparedStatement;
      PreparedStatement doctorPreparedStatement;
      try {
        userDetailUpdatePreparedStatement =
                hospitalDbConnection.prepareStatement(UserDetailsQueries.UPDATE_BY_ID);
        DaoUtil.prepareStatementSetUserDetailsForUpdate(userDetailUpdatePreparedStatement,
                newDoctor);
        int rowsAffected = userDetailUpdatePreparedStatement.executeUpdate();
        if (rowsAffected == 1) {
          doctorPreparedStatement =
                  hospitalDbConnection.prepareStatement(DoctorQuries.DOCTOR_UPDATE_BY_ID);
          prepareStatementForDoctorUpdate(doctorPreparedStatement, newDoctor);
          int rowsAffectedInDoctor = doctorPreparedStatement.executeUpdate();
          return DaoUtil.checkRowsAffected(hospitalDbConnection, rowsAffectedInDoctor);
        } else {
          hospitalDbConnection.rollback();
          return false;
        }
      } catch (SQLException exception) {
        hospitalDbConnection.rollback();
        throw new SQLException(exception);
      }


    } else {
      throw new DoctorNotFoundException(ApplicationConstant.DOCTOR_NOT_FOUND);
    }

  }


  /**
   * Gets user id by doctor id.
   *
   * @param doctorId the doctor id
   * @return the user id by doctor id
   * @throws ClassNotFoundException the class not found exception
   * @throws SQLException           the sql exception
   * @throws InstantiationException the instantiation exception
   * @throws IllegalAccessException the illegal access exception
   */
  public int getUserIdByDoctorId(int doctorId) throws ClassNotFoundException, SQLException,
          InstantiationException, IllegalAccessException {
    LOGGER.trace(Integer.toString(doctorId));
    Connection dbConnection = DbConfig.getHmsDaoConnection();
    PreparedStatement preparedStatement =
            dbConnection.prepareStatement(DoctorQuries.GET_USER_ID_BY_PATIENT_ID);
    preparedStatement.setInt(NumericConstants.ONE, doctorId);
    ResultSet resultSet = preparedStatement.executeQuery();
    resultSet.next();
    int userId = resultSet.getInt(NumericConstants.ONE);
    return userId;
  }

  /**
   * prepare a statment for update .
   *
   * @param preparedStatement preparedStatment
   * @param doctor            doctor
   * @throws SQLException while query fails
   */
  private void prepareStatementForDoctorUpdate(PreparedStatement preparedStatement,
                                               Doctor doctor) throws SQLException {
    preparedStatement.setString(1, doctor.getDoctor_specialization());
    preparedStatement.setInt(2, doctor.getDoctorId());

  }


  /**
   * Gets all patients.
   *
   * @param doctorId the doctor id
   * @return the all patients
   * @throws ClassNotFoundException the class not found exception
   * @throws SQLException           the sql exception
   * @throws InstantiationException the instantiation exception
   * @throws IllegalAccessException dsadas illegal access exception
   */
  public DoctorPatientMapper getAllPatients(int doctorId) throws ClassNotFoundException,
          SQLException, InstantiationException, IllegalAccessException {
    LOGGER.traceEntry(Integer.toString(doctorId));
    Connection dbConnection = DbConfig.getHmsDaoConnection();
    PreparedStatement preparedStatement =
            dbConnection.prepareStatement(DoctorQuries.GET_PATIENTS_OF_DOCTOR);
    DoctorPatientMapper doctorPatientMapper;
    preparedStatement.setInt(NumericConstants.ONE, doctorId);
    ResultSet resultSet = preparedStatement.executeQuery();
    doctorPatientMapper = makeResultSetAsPatietnsRecord(doctorId, resultSet);
    if (doctorPatientMapper != null) {
      LOGGER.traceExit(doctorPatientMapper);
      return doctorPatientMapper;
    } else {
      LOGGER.traceExit(null);
      return null;
    }

  }

  /**
   * making resultAs Doctorpatient mpper .
   *
   * @param doctorId  doctorId
   * @param resultSet resultSer
   * @return Doctopatientmapper
   * @throws SQLException sqlException
   */
  private DoctorPatientMapper makeResultSetAsPatietnsRecord(int doctorId, ResultSet resultSet) throws SQLException {
    LOGGER.traceEntry(Integer.toString(doctorId), resultSet);
    DoctorPatientMapper doctorPatientMapper = new DoctorPatientMapper();
    doctorPatientMapper.setDoctorId(doctorId);
    List<Patient> patients = new ArrayList<Patient>();
    while (resultSet.next()) {
      Patient patient = new Patient();
      patient.setUsername(resultSet.getString(EntityConstant.USERNAME));
      patient.setUserFirstName(resultSet.getString(EntityConstant.FIRSTNAME));
      patient.setPatientId(resultSet.getInt(EntityConstant.PATIENT_ID));
      patient.setUserId(resultSet.getInt(EntityConstant.USER_ID));
      patient.setUserLastName(resultSet.getString(EntityConstant.LASTNAME));
      patient.setBloodGroup(resultSet.getString(EntityConstant.BLOOD_GROUP));
      patient.setWeight(resultSet.getInt(EntityConstant.WEIGHT));
      patient.setUserCity(resultSet.getString(EntityConstant.CITY));
      patient.setUserState(resultSet.getString(EntityConstant.STATE));
      patient.setPhoneNumber(resultSet.getString(EntityConstant.PHONE_NUMBER));
      patients.add(patient);
    }

    if (patients.size() > 0) {
      doctorPatientMapper.setPatients(patients);
      LOGGER.traceExit(doctorPatientMapper);
      return doctorPatientMapper;
    } else {
      LOGGER.traceExit(null);
      return null;
    }

  }
}
